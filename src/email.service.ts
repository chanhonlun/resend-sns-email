import axios from 'axios';
import * as escape from 'escape-html';

import { config } from './helpers';

export async function sendEmail({
  from,
  to,
  cc,
  bcc,
  replyTo,
  date,
  subject,
  html,
}: {
  from: string;
  to: string[];
  cc?: string[];
  bcc?: string[];
  replyTo?: string;
  date: Date;
  subject: string;
  html: string;
  attachments?: any[];
}) {
  const response = await axios({
    baseURL: config('email.server'),
    url: '/email/schedule',
    method: 'post',
    data: {
      sendAt: new Date().toISOString(),
      email: {
        from: 'default',
        to: config('email.receiver'),
        subject: `A new email received`,
        html: `
          <div>email information:</div>
          <ul>
            <li>From: ${escape(from)}</li>
            <li>To: ${to.map(escape).join(', ')}</li>
            <li>Cc: ${cc.map(escape).join(', ')}</li>
            <li>Bcc: ${bcc.map(escape).join(', ')}</li>
            <li>Reply To: ${replyTo ? escape(replyTo) : ''}</li>
            <li>Date: ${date.toISOString()}</li>
            <li>subject: ${subject}</li>
          </ul>
          <hr />
          ${html}
        `,
      },
    },
  });

  return response.data;
}
