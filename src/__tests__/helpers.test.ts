import * as plainTextEvent from './sns-notification.plain-text.json';
import * as ccBccEvent from './sns-notification.cc-and-bcc.json';
import * as attachmentEvent from './sns-notification.attachments.json';
import { parseContent } from '../helpers';

describe('parseContent', () => {
  const getRawContent = (event) =>
    JSON.parse(event.Records[0].Sns.Message).content;

  test('email with plain text', async () => {
    const {
      from,
      to,
      cc,
      bcc,
      replyTo,
      date,
      subject,
      html,
      attachments,
    } = await parseContent(getRawContent(plainTextEvent));

    expect(from).toEqual('resend.sns.email <resend.sns.email@protonmail.com>');
    expect(to).toHaveLength(1);
    expect(to[0]).toEqual('no-reply@example.com');
    expect(cc).toHaveLength(0);
    expect(bcc).toHaveLength(0);
    expect(replyTo).toEqual(
      'resend.sns.email <resend.sns.email@protonmail.com>'
    );
    expect(date).toEqual(new Date('2021-04-05T23:45:56+0800'));
    expect(subject).toEqual('Test plain text');
    expect(html).toEqual('<div>hello world!</div>');
    expect(attachments).toHaveLength(0);
  });

  test('email with cc and bcc', async () => {
    const {
      from,
      to,
      cc,
      bcc,
      replyTo,
      date,
      subject,
      html,
      attachments,
    } = await parseContent(getRawContent(ccBccEvent));

    expect(from).toEqual('resend.sns.email <resend.sns.email@protonmail.com>');
    expect(to).toHaveLength(1);
    expect(to[0]).toEqual('no-reply1@example.com');
    expect(cc).toHaveLength(1);
    expect(cc[0]).toEqual('no-reply2@example.com');
    expect(bcc).toHaveLength(0);
    expect(replyTo).toEqual(
      'resend.sns.email <resend.sns.email@protonmail.com>'
    );
    expect(date).toEqual(new Date('2021-04-06T14:12:35+0800'));
    expect(subject).toEqual('Test cc and bcc');
    expect(html).toEqual('<div>hello world</div>');
    expect(attachments).toHaveLength(0);
  });

  test('email with attachment', async () => {
    const {
      from,
      to,
      cc,
      bcc,
      replyTo,
      date,
      subject,
      html,
      attachments,
    } = await parseContent(getRawContent(attachmentEvent));

    expect(from).toEqual('resend.sns.email <resend.sns.email@protonmail.com>');
    expect(to).toHaveLength(1);
    expect(to[0]).toEqual('no-reply@example.com');
    expect(cc).toHaveLength(0);
    expect(bcc).toHaveLength(0);
    expect(replyTo).toEqual(
      'resend.sns.email <resend.sns.email@protonmail.com>'
    );
    expect(date).toEqual(new Date('2021-04-05T23:49:25+0800'));
    expect(subject).toEqual('Test attachments and inline images');
    expect(html).toContain('some image: &lt;<img');
    expect(html).toContain('data:image/png;base64');
    expect(attachments).toHaveLength(2);

    expect(attachments[0].filename).toEqual(
      '螢幕截圖 2020-01-15 上午12.35.26.png'
    );
    expect(attachments[0].contentType).toEqual('image/png');
    expect(attachments[0].size).toEqual(33297);

    expect(attachments[1].filename).toEqual('text-file.txt');
    expect(attachments[1].contentType).toEqual('text/plain');
    expect(attachments[1].size).toEqual(12);
    expect(Buffer.from(attachments[1].content).toString()).toEqual(
      'hello world\n'
    );
  });
});
