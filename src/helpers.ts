import { get } from 'lodash';
import { AddressObject, ParsedMail, simpleParser } from 'mailparser';

import AppConfig from './config';

export function config<T>(path: string, _default?: T): T {
  return get(AppConfig, path, _default);
}

export function ensureArray<T>(arg: T | T[]): T[] {
  if (arg == undefined) return [];
  return Array.isArray(arg) ? arg : [arg];
}

export async function parseContent(
  rawContent: string
): Promise<{
  from: AddressObject['text'];
  to: AddressObject['text'][];
  cc: AddressObject['text'][];
  bcc: AddressObject['text'][];
  replyTo?: AddressObject['text'];
  date: ParsedMail['date'];
  subject: ParsedMail['subject'];
  html: string;
  attachments: ParsedMail['attachments'];
}> {
  const parsed = await simpleParser(rawContent);

  const {
    from,
    to,
    cc,
    bcc,
    replyTo,
    date,
    subject,
    html,
    textAsHtml,
    attachments,
  } = parsed;

  return {
    from: from.text,
    to: ensureArray(to).map((addr) => addr.text),
    cc: ensureArray(cc).map((addr) => addr.text),
    bcc: ensureArray(bcc).map((addr) => addr.text),
    replyTo: replyTo?.text,
    date,
    subject,
    html: html ? html : textAsHtml ? textAsHtml : 'no html',
    attachments: attachments.filter(
      (attachment) => attachment.contentDisposition === 'attachment'
    ),
  };
}
