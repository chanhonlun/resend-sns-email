import { parseContent } from './helpers';
import { sendEmail } from './email.service';

export const handler = async (event) => {
  console.log('event', JSON.stringify(event));

  const snsRecord = event.Records.find(
    (record) => record.EventSource === 'aws:sns'
  );

  if (!snsRecord) return;

  const snsMessage = JSON.parse(snsRecord.Sns.Message);

  const parsed = await parseContent(snsMessage.content);

  const response = sendEmail(parsed);

  console.log('response', response);

  return response;
};
