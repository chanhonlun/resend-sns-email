export default {
  email: {
    server: process.env.EMAIL_SERVER ?? 'https://localhost:4000',
    receiver: process.env.EMAIL_RECEIVER,
  },
};
